Feature('Acceptance Test');

const { I, constant, credentials, signIn, projects} = inject();

BeforeSuite(async () => {
    await signIn.openSignIn();
    signIn.checkLoginForm();
    signIn.signInBy(credentials.projectAccount);
    await projects.openProjectsPage();
    projects.clearProjects();
});

Scenario('Case 1: Add 1st project @acceptance', async () => {
    await projects.openProjectsPage();
    await projects.callCreatePorjectModal();
    projects.fillCreateProjectModal(constant.project1.name);
    projects.openProjectsPage();
    projects.checkOffersOrder([constant.project1.name]);
});

Scenario('Case 2: Add nth project @acceptance', async () => {
    await projects.openProjectsPage();
    await projects.callCreatePorjectModal();
    projects.fillCreateProjectModal(constant.project2.name);
    projects.openProjectsPage();
    projects.checkOffersOrder([constant.project1.name, constant.project2.name]);
});

Scenario('Case 3: Add 1st key @acceptance', async () => {
    await projects.openProjectsPage();
    projects.openCreatedProject();
    projects.callCreateKeyModal();
    projects.fillCreateKeyModal(constant.project1.key1Name,'Web');
});

Scenario('Case 4: Add nth key @acceptance', async () => {
    await projects.openProjectsPage();
    projects.openCreatedProject();
    projects.callCreateKeyModal();
    projects.switchToPlural();
    projects.fillCreateKeyModal(constant.project1.key2Name,'Web');
});

Scenario('Case 5: Add translation for plain key @acceptance', () => {
    projects.openProjectsPage();
    projects.openCreatedProject();
    projects.updatePlainKey(constant.project1.key1Name, 1, 'value 1');
});

Scenario('Case 6: Update translation for plain key @acceptance', () => {
    projects.openProjectsPage();
    projects.openCreatedProject();
    projects.editPlainKey(constant.project1.key1Name, 'value 1', '2');
});

Scenario('Case 5: Add translation for plural key @acceptance', () => {
    projects.openProjectsPage();
    projects.openCreatedProject();
    projects.updatePlainKey(constant.project1.key2Name, 1, 'value 1');
    projects.updatePlainKey(constant.project1.key2Name, 2, 'value 2');
});