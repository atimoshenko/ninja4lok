const environment = process.env.npm_package_config_env || 'app';
const iuiUrl = process.env.iui || `https://${environment}.lokalise.com`;
const suite = process.env.suite || 'acceptance';

const path = require('path');
const rootDir = process.cwd();

const arguments = (mode = '') => {
    let arguments = ["--window-size=1680,1050", "--incognito"];
    if (mode === 'headless') {
        arguments.push("--headless");
        arguments.push("--no-sandbox");
        arguments.push("--disable-dev-shm-usage");
        arguments.push("--disable-web-security");
    }
    return arguments
};

let show = (mode = '') => mode !== 'headless';

exports.config = {
    name: 'ninja',
    tests: path.resolve(rootDir, `./suites/test/*_test.js`),
    output: path.resolve(rootDir, `./output/${suite}`),
    helpers: {
        Puppeteer: {
            url: iuiUrl,
            restart: false,
            keepCookies: true,
            keepBrowserState: true,
            waitForAction: 1100,
            // waitForNavigation: "networkidle0",
            show: show(process.env.mode),
            waitForTimeout: 25000,
            windowSize: '1680x760',
            chrome: {
                args: arguments(process.env.mode),
                prefs: {
                    'intl.accept_languages': 'en-US',
                    'download': {
                        'default_directory': path.join(rootDir, './data/temp'),
                        'download_restrictions': 0,
                        'extensions_to_open': '',
                        'directory_upgrade': true,
                        'prompt_for_download': false,
                        'safebrowsing.enabled': false,
                        'safebrowsing.disable_download_protection': true,
                    },
                    'safebrowsing': {
                        'enabled': true,
                    }
                }
            }
        },
        Mochawesome: {
            uniqueScreenshotNames: true
        }

    },
    plugins: {
        /** pauseOnFail hook is a codeceptjs hook to for debugging - automation will pause on any failure **/
        /** enable it to debug locally **/
        // pauseOnFail: {
        //     enabled: true
        // },
        tryTo: {
            enabled: true,
        },
        autoDelay: {
            enabled: true,
        },
        retryFailedStep: {
            enabled: true
        },
        screenshotOnFail: {
            enabled: true
        },
    },
    include: {
        credentials: path.join(rootDir, './suites/config/credentials.js'),
        constant: path.join(rootDir, './suites/config/const.js'),
        ...require(path.join(rootDir, './pageModel/_pages.js')),
    },
    mocha: {
        reporterOptions: {
            reportDir: path.join(rootDir, `./output/${suite}/report`),
            reportFilename: `${environment}-${suite}-report`,
            reportPageTitle: `Report from ${environment}`,
            inlineAssets: true,
            json: false,
        }
    },
    bootstrap: false,
};
