'use strict';

const { I } = inject();

module.exports = {


    dashboard: {
        addProjectLink: { xpath: '//a[contains(text(),"adding a project")]' },
        newProjectButton: { css: '[data-action="add-project"]' },
        proj: { xpath: '//a[contains(@data-name,"project-name")]' },
        projectName(i) { return `//div[${i}]/div/div/a[contains(@data-name,"project-name")]` },
        projectSettingsButton: { xpath: '//body/div[1]/main[1]/div[2]/div[1]/div[2]/project-list[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[3]/div[2]/div[1]/*[1]' },
        settingsDropdownButton: { xpath: '//a[contains(text(),"Settings")]' }
    },

    sideBar: {
        projectsNavigationLink: { css: '[href="/projects"]' }
    },

    createProjectModal: {
        projectNameInput: { css: '[id="project-name"]' },
        projectAddButton: { css: '[id="project-add"]' }
    },

    project: {
        projectName: { css: '[class="project-title-wrapper"]' },
        addKeyButton: { css: '[class="btn btn-sm btn-primary addkey-btn add-key-trigger"]'},
        keyName(keyName) { return `[data-value="${keyName}"]` },
        emptyKeyValue(keyName,id) { return `//div[@data-name="${keyName}"]//td[2]/div[1]/div[1]/div[${id}]//span[@class="empty"]`},
        filledKeyValue(keyName,text) { return `//div[@data-name="${keyName}"]//*[text()="${text}"]`},
        textArea(keyName, oldName) { return `//div[@data-name="${keyName}"]//span[text()="${oldName}"]` },
        saveValueButton: { css: '[class="btn btn-primary btn-sm editor-icon-button save"]' },
        deleteProjectButton: { css: '[class="btn btn-danger btn-outlined project-delete"]' },
        deleteProjectNameInput: {  xpath: '//input[@id="branch-name"]' },
        deleteProjectRealButton: {  xpath: '//button[contains(text(),"Delete project")]' },
    },

    createKeyModal: {
        keyNameInput: { css: '[id="keyName"]' },
        platformsDropdown: { css: '[id="s2id_autogen11"]'},
        platformsDropdownWebItem(element) { return `//div[text()="${element}"]` },
        keyAddButton: { css: '[id="btn_addkey"]' },
        generalTab: { css: '[id="general_tab"]' },
        advancedTab: { css: '[id="advanced_tab"]' },
        pluralRadioButton: { xpath: '//body/div[1]/main[1]/div[2]/div[1]/div[15]/div[1]/div[1]/div[2]/form[1]/div[1]/div[8]/div[1]/div[1]/div[1]/div[1]/span[3]'}

    },

    openProjectsPage() {
        I.amOnPage('/projects');
    },


    async callCreatePorjectModal() {
        const dashboard = this.dashboard;

        I.amOnPage('/projects');
        let emptyDashboardState = await I.grabNumberOfVisibleElements(dashboard.newProjectButton);

        if (emptyDashboardState === 1) {
            I.waitForElement(dashboard.newProjectButton);
            I.click(dashboard.newProjectButton);
        } else {
            I.waitForElement(dashboard.addProjectLink);
            I.click(dashboard.addProjectLink);
        }
    },

    async clearProjects() {
        const dashboard = this.dashboard;
        const project = this.project;

            const projectsQuantity = await I.grabNumberOfVisibleElements(dashboard.proj);
            for (let i = 0; i < projectsQuantity; i++ ) {
                I.waitForVisible(dashboard.proj);
                let name = await I.grabTextFrom(dashboard.proj);
                I.waitForVisible(dashboard.projectSettingsButton);
                I.click(dashboard.projectSettingsButton);
                I.waitForVisible(dashboard.settingsDropdownButton);
                I.click(dashboard.settingsDropdownButton);
                I.waitForVisible(project.deleteProjectButton);
                I.click(project.deleteProjectButton);
                I.waitForEnabled(project.deleteProjectNameInput);
                I.fillField(project.deleteProjectNameInput, name);
                I.waitForEnabled(project.deleteProjectRealButton);
                I.click(project.deleteProjectRealButton)
            }
    },

    fillCreateProjectModal(projectName) {
        const createProjectModal = this.createProjectModal;
        const project = this.project;

        I.waitForVisible(createProjectModal.projectNameInput);
        I.waitForVisible(createProjectModal.projectAddButton);
        I.fillField(createProjectModal.projectNameInput, projectName);
        I.click(createProjectModal.projectAddButton);
        I.seeTextEquals(projectName, project.projectName);
    },

    async checkOffersOrder(projectName) {
        const dashboard = this.dashboard;

        for (let i = 1; i < projectName.length + 1; i++) {
            I.seeTextEquals(projectName[i-1], dashboard.projectName(i));
        }
    },

    openCreatedProject() {
        const dashboard = this.dashboard;

        I.click(dashboard.projectName(1));
    },

    callCreateKeyModal() {
        const project = this.project;

        I.waitForVisible(project.addKeyButton);
        I.click(project.addKeyButton);
    },

    async fillCreateKeyModal(keyName,element) {
        const createKeyModal = this.createKeyModal;
        const project = this.project;

        I.click(createKeyModal.generalTab);
        I.waitForEnabled(createKeyModal.keyNameInput);
        I.fillField(createKeyModal.keyNameInput, keyName);
        const platformsQuantity = await I.grabNumberOfVisibleElements(createKeyModal.platformsDropdownWebItem(element));
        if (platformsQuantity === 0) {
            I.click(createKeyModal.platformsDropdown);
            I.waitForVisible(createKeyModal.platformsDropdownWebItem(element));
            I.click(createKeyModal.platformsDropdownWebItem(element));
        }
            I.click(createKeyModal.keyAddButton);
            I.refreshPage();
            I.waitForVisible(project.keyName(keyName));
    },

    switchToPlural() {
        const createKeyModal = this.createKeyModal;

        I.click(createKeyModal.advancedTab);
        I.waitForEnabled(createKeyModal.pluralRadioButton);
        I.click(createKeyModal.pluralRadioButton);
    },


    async checkKeysOrder(keyName) {
        const project = this.project;

        I.refreshPage();
        for (let i = 1; i < keyName.length + 1; i++) {
            I.seeTextEquals(keyName[i-1], project.keyName(i));
        }
    },

    updatePlainKey(keyName, id, text) {
        const project = this.project;

        I.waitForVisible(project.emptyKeyValue(keyName, id));
        I.click(project.emptyKeyValue(keyName, id));
        I.type(text, 100);
        I.waitForEnabled(project.saveValueButton);
        I.click(project.saveValueButton);
        I.waitForVisible(project.filledKeyValue(keyName, text));
    },

    editPlainKey(keyName, oldName, text) {
        const project = this.project;

        I.waitForVisible(project.filledKeyValue(keyName, oldName));
        I.click(project.filledKeyValue(keyName, oldName));
        I.pressKey('Backspace');
        I.type(text, 100);
        I.waitForEnabled(project.saveValueButton);
        I.click(project.saveValueButton);
        I.waitForVisible(project.filledKeyValue(keyName, 'value ' + text));
    },
};
