'use strict';

const { I } = inject();

module.exports = {

    signIn: {
        emailInput: { css: '[id="signin-email"]' },
        passwordInput: { css: '[id="signinPassword"]' },
        loginButton: { css: '[id="go-login"]' },
        ssoLoginLink: { css: '[href="/sso"]' },
        forgotPasswordLink: { css: '[id="open-reset-password"]' },
        notRegisteredLink: { css: '[href="/signup"]' }
    },

    openSignIn() {
        I.amOnPage('');
    },

    checkLoginForm() {
        const signIn = this.signIn;

        I.waitForVisible(signIn.emailInput);
        I.waitForVisible(signIn.passwordInput);
        I.waitForVisible(signIn.ssoLoginLink);
        I.waitForVisible(signIn.forgotPasswordLink);
        I.waitForVisible(signIn.notRegisteredLink);
        I.waitForVisible(signIn.loginButton);
    },

    signInBy(credentials) {
        const signIn = this.signIn;

        I.waitForVisible(signIn.emailInput);
        I.fillField(signIn.emailInput, credentials.email);
        I.fillField(signIn.passwordInput, credentials.password);
        I.waitForVisible(signIn.loginButton);
        I.click(signIn.loginButton);
    }
};
