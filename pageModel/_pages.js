const path = require('path');
const rootDir = process.cwd();

module.exports = {
    signIn: path.join(rootDir, './pageModel/signIn.js'),
    projects: path.join(rootDir, './pageModel/projects.js')
};
