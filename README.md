**Requirements:**
For using test framework NodeJS and Java and Chromium are needed.
Download NodeJS 11 here: https://nodejs.org/en/
Download Java 8 or higher here: tps://www.java.com
Download Chromium here: https://www.chromium.org/

**Getting Started:**
To install all dependencies go to framework root and execute command:
`npm install`

---

**Command To Run:**

1. To run test in report mode:
`env=(see options) suite=(see options) npm run test`

**Commands For Developers:**

2. To run test in steps mode:
`env=(see options) suite=(see options) output=steps npm run test`

3. To run test in verbose mode:
`env=(see options) suite=(see options) output=verbose npm run test`

**Options:**

`env` - environment to be tested
options:
* `app` (default)

`suite` - test suite to run
options:
* `acceptance` (default)


`output` - console output option, which is html report on default
options:
* `report` (default)
* `steps`
* `debug`
* `verbose` 
